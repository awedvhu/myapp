﻿using _0505.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _0505.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index() {
            return View(new BMIData());
        }
        // GET: BMI
        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (data.Height < 50 || data.Height > 200) {
                ViewBag.HeightError = "身高請輸入50~200的數值";
            }
            if (data.Weight < 30 || data.Weight > 150)
            {
                ViewBag.WeightError = "體重請輸入30~150的數值";
            }
            if (ModelState.IsValid)
            {
                var m_height = data.Height.Value / 100;
                var result = data.Weight / (m_height * m_height);

                data.Result = result;
                var level = "";
                if (result < 18.5)
                {
                    level = "體重過輕";
                }
                else if (result >= 18.5 && result < 24)
                {
                    level = "正常範圍";
                }
                else if (result >= 24 && result < 27)
                {
                    level = "過重";
                }
                else if (result >= 27 && result < 30)
                {
                    level = "輕度肥胖";
                }
                else if (result >= 30 && result < 35)
                {
                    level = "中度肥胖";
                }
                else
                {
                    level = "重度肥胖";
                }
                data.Level = level;
                
            }
            
            return View(data);
        }
    }
}
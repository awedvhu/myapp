﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _0505.Models.partial
{   [MetadataType(typeof(UsersMetadata))]
    public partial class Users
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public System.DateTime Birthday { get; set; }
        public bool Gender { get; set; }
    }

    public class UsersMetadata {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public System.DateTime Birthday { get; set; }
        public bool Gender { get; set; }
    }
}